
" """"""""""""""""""""""""""""""""""""""""
inoremap jj <esc>

" """"""""""""""""""""""""""""""""""""""""
" Disable compatibility with vi which can cause unexpected issues.
set nocompatible

" Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on

" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on



" """"""""""""""""""""""""""""""""""""""""
" Show the mode you are on the last line.
set showmode
" Show partial command you type in the last line of the screen.
set showcmd
" Set the commands to save in history default number is 20.
set history=1000

" """"""""""""""""""""""""""""""""""""""""
" Turn syntax highlighting on.
syntax on
" Use space characters instead of tabs.
set expandtab

" Add numbers to each line on the left-hand side.
set number
" Highlight cursor line underneath the cursor horizontally.
set cursorline
" Highlight cursor line underneath the cursor vertically.
"set cursorcolumn
" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=3


" """"""""""""""""""""""""""""""""""""""""
" Some options for searching with /

" Ignore capital letters during search.
set ignorecase
" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase


" While searching though a file incrementally highlight matching characters as you type.
set incsearch
" Use highlighting when doing a search.
set hlsearch
" Show matching words during a search.
set showmatch


" """"""""""""""""""""""""""""""""""""""""

" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
"set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx



" """"""""""""""""""""""""""""""""""""""""

" set nocursorline

set termguicolors
set background=dark " for the dark version
" set background=light " for the light version

" colorscheme one
colorscheme molokai
" colorscheme gruvbox
" colorscheme neverland

" colorscheme hybrid_reverse
" colorscheme nightfly
" colorscheme kit




" """"""""""""""""""""""""""""""""""""""""
"plugin section

"- vim airline
"- airline-themes
"- (i.was mit latex)
"- commentary.vim
"- nerdtree
"- nerdtree-git-plugin
"- fzf!?
"- (surround.vim - completes the matching parentheses)
"- utilsnip
"- jake hat außerdem: gitgutter, fugitive, hardmode, ctrlp vim, zoomwin

" PLUGINS ---------------------------------------------------------------- {{{

" Plugin code goes here.
call pathogen#infect()


" NERDTree config ==================================================
" show hidden files in NERDTree
let NERDTreeShowHidden=1
" Start NERDTree and put the cursor back in the other window.
autocmd VimEnter * NERDTree | wincmd p
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif


" vim-latex-live-preview configs ========================================
autocmd Filetype tex setl updatetime=1000
" let g:livepreview_previewer = 'zathura'
" let g:livepreview_previewer = 'mupdf'
let g:livepreview_previewer = 'evince'
let g:livepreview_engine = 'xelatex'


" typst.vim configs ========================================
let g:typst_pdf_viewer = 'mupdf'

" }}}




" MAPPINGS --------------------------------------------------------------- {{{

" Mappings code goes here.
" cycle through tabs
nnoremap <C-c> <C-PageDown>
nnoremap <C-t> <C-PageUp>

" Change to visual mode from insert mode directly
inoremap <C-v> <esc>v


" }}}


" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding.
" Use the marker method of folding.
"augroup filetype_vim
"    autocmd!
"    autocmd FileType vim setlocal foldmethod=marker
"augroup END


" With this feature enabled, Vim automatically saves history to external
" files, making the undo/redo functionality continue to work after restarting
" Vim or closing a buffer.
" Use persistent history.
if !isdirectory("/tmp/.vim-undo-dir")
    call mkdir("/tmp/.vim-undo-dir", "", 0700)
endif
set undodir=/tmp/.vim-undo-dir
set undofile


" Map the F5 key to run a Python script inside Vim.
" I map F5 to a chain of commands here.
" :w saves the file.
" <CR> (carriage return) is like pressing the enter key.
" !clear runs the external clear screen command.
" !python3 % executes the current file with Python.
nnoremap <f5> :w <CR>:!clear <CR>:!python3 % <CR>


" UltiSnips shortcuts

let g:UltiSnipsExpandTrigger="<C-f>"
let g:UltiSnipsJumpForwardTrigger="<C-f>"
let g:UltiSnipsJumpBackwardTrigger="<C-d>"

let g:UltiSnipsListSnippets="<C-t>"


" markdown-preview.vim
nnoremap <C-m> :MarkdownPreviewToggle<CR>


" ale config
let g:ale_linters = {
\   'javascript': ['eslint', 'prettier'],
\   'typescript': ['eslint', 'prettier'],
\   'css': ['prettier'],
\   'html': ['prettier'],
\   'json': ['prettier'],
\}
let g:ale_fixers = {
\   'javascript': ['eslint', 'prettier'],
\   'typescript': ['eslint', 'prettier'],
\   'css': ['prettier'],
\   'html': ['prettier'],
\   'json': ['prettier'],
\}

let g:ale_lint_on_save = 1
let g:ale_fix_on_save = 1


" }}}

" AUTOCOMMANDS ---------------------------------------------------------------- {{{
augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

augroup END


augroup my_general
    au InsertEnter * :set norelativenumber
    au InsertLeave * :set relativenumber
augroup END


augroup VIMPROVEMENT
    autocmd!
    " Reload my vimrc everytime I save it.
    autocmd BufWritePost ~/.vimrc source % | redraw | echo "Reloaded vimrc!"

    " highlight yank
    au TextYankPost * silent! lua vim.highlight.on_yank {higroup="IncSearch", timeout=300}

augroup END


augroup filetype_python
    autocmd!
    " This only sets the type for files which don't have a filetype.
    autocmd BufRead,BufNewFile *.py setfiletype python
    autocmd FileType python set expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
augroup END

augroup filetype_web
    autocmd!
    " This only sets the type for files which don't have a filetype.
    autocmd BufRead,BufNewFile *.js setfiletype javascript
    autocmd BufRead,BufNewFile *.ts setfiletype typescript
    autocmd BufRead,BufNewFile *.html setfiletype html
    autocmd BufRead,BufNewFile *.css setfiletype css
    autocmd FileType javascript set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType typescript set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType html set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType css set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType javascript, typescript setlocal commentstring=//\ %s
augroup END

augroup filetype_latex
    autocmd!
    " This only sets the type for files which don't have a filetype.
    autocmd BufRead,BufNewFile *.tex setfiletype tex
    autocmd FileType tex set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
augroup END

augroup filetype_typst
    autocmd!
    " This only sets the type for files which don't have a filetype.
    autocmd BufRead,BufNewFile *.typ setfiletype typst
    autocmd FileType typst set expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
augroup END

augroup VIM
    autocmd!
    autocmd FileType vim set expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
augroup END

augroup filetype_c
    autocmd!
    autocmd FileType c setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
    autocmd FileType cpp setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
    autocmd FileType c,cpp setlocal commentstring=//\ %s
augroup END

augroup filetype_java
    autocmd!
    autocmd FileType java setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
augroup END

augroup filetype_various
    autocmd!
    autocmd FileType zsh setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
    autocmd FileType bash setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
    autocmd FileType yaml setlocal expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType markdown setlocal expandtab tabstop=2 shiftwidth=2 autoindent smartindent smarttab
    autocmd FileType json setlocal expandtab tabstop=4 shiftwidth=4 autoindent smartindent smarttab
augroup END

" }}}


"Toggle YouCompleteMe on and off with F3
function Toggle_ycm()
    if g:ycm_show_diagnostics_ui == 0
        let g:ycm_auto_trigger = 1
        let g:ycm_show_diagnostics_ui = 1
        :YcmRestartServer
        :e
        :echo "YCM on"
    elseif g:ycm_show_diagnostics_ui == 1
        let g:ycm_auto_trigger = 0
        let g:ycm_show_diagnostics_ui = 0
        :YcmRestartServer
        :e
        :echo "YCM off"
    endif
endfunction
nnoremap ycm :call Toggle_ycm() <CR>


set spell spelllang=en_us

