# vim-config

my vim config files

## Installation

First of all: Install vim. And don't forget the gtk3 version for clipboard support, if you want that :)
```bash
sudo apt-get install vim vim-gtk3
```

To clone this repo with all its submodules (the plugins) use the following command:
```bash
git clone --recurse-submodules git@gitlab.gwdg.de:janphilipp.ressler/vim-config.git .vim
```

In order to let this repo take effects the `vimrc` file needs to be executed. Therefore simply delete your `.vimrc` file in your home directory.
```bash
rm ~/.vimrc
```

## Updating

If you've already cloned the repo and want to update the submodules inside, use this command:
```bash
git submodule update --init --recursive
git submodule foreach --recursive git pull origin master
```

## Add Plugins

Go to .vim/bundle and type `git submodule add <link>`.

Also follow the instructions on the homepage of the plugin.


### YouCompleteMe

You need to install YCM on each device. Please refer to the installation instructions for linux:
https://github.com/ycm-core/YouCompleteMe

In essence you need to install some packages for some languages and then compile YCM:

```bash
sudo apt-get install build-essential cmake vim-nox python3-dev
sudo apt-get install mono-complete # for C# and .NET support
sudo apt-get install golang
sudo apt-get install nodejs npm # !!! please use the nvm installing steps instead
sudo apt-get install openjdk-21-jdk openjdk-21-jre
```
and
```bash
cd ~/.vim/bundle/YouCompleteMe && python3 install.py --all
```

### Markdown-Preview ###

In order to use this plugin you need to install nodejs and npm (the homepage says yarn but that does'nt work for me). Go into the folder of this repo and execute `npm install`.

### ALE
very nice linting plugin.
To use linters the actual linters itself must be installed first.

e.g. standard via `npm install -g standard`  
https://github.com/standard/standard?tab=readme-ov-file#install
and tsserver with  
`npm install -g typescript-language-server typescript`



