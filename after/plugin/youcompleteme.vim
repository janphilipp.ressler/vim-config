
au VimEnter * call s:customize_ycm_options()

function! s:customize_ycm_options()
    if exists('g:ycm_filetype_blacklist')
        let g:ycm_filetype_blacklist.markdown = 0
    endif
    if exists('g:ycm_filetype_whitelist')
        let g:ycm_filetype_whitelist.markdown = 1
    endif
endfunction




